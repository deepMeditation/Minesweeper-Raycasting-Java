package main.raycasting;

import java.awt.*;
import java.util.ArrayList;

public class Screen {
    private int[][] map;
    private ArrayList<Texture> textures = Texture.getAll();
    public int mapWidth, mapHeight, width, height;

    public Screen(int[][] map, int mapWidth, int mapHeight, int width, int height) {
        this.map = map;
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
        this.width = width;
        this.height = height;
    }

    public int[] update(Player player, int[] pixels) {
        // paint upper half darker
        for (int n = 0; n < pixels.length / 2; n++)
            if (pixels[n] != Color.DARK_GRAY.getRGB()) pixels[n] = Color.DARK_GRAY.getRGB();
        // paint lower half brighter
        for (int i = pixels.length / 2; i < pixels.length; i++)
            if (pixels[i] != Color.gray.getRGB()) pixels[i] = Color.gray.getRGB();

        for (int x = 0; x < width; x++) {
            double cameraX = 2 * x / (double) (width) - 1;
            double rayDirX = player.xDir + player.xPlane * cameraX;
            double rayDirY = player.yDir + player.yPlane * cameraX;

            RayHit hit = castRay(rayDirX, rayDirY, player.xPos, player.yPos);

            //Calculate distance to the point of impact
            double perpWallDist;
            if (hit.side == 0) {
                perpWallDist = Math.abs((hit.mapX - player.xPos + (1 - hit.stepX) / 2) / rayDirX);
            } else {
                perpWallDist = Math.abs((hit.mapY - player.yPos + (1 - hit.stepY) / 2) / rayDirY);
            }

            //Now calculate the height of the wall based on the distance from the player
            int lineHeight;
            if (perpWallDist > 0) {
                lineHeight = Math.abs((int) (height / perpWallDist));
            } else {
                lineHeight = height;
            }

            //calculate lowest and highest pixel to fill in current stripe
            int drawStart = -lineHeight / 2 + height / 2;
            if (drawStart < 0)
                drawStart = 0;

            int drawEnd = lineHeight / 2 + height / 2;
            if (drawEnd >= height)
                drawEnd = height - 1;

            //add a texture
            double wallX;//Exact position of where wall was hit
            if (hit.side == 1) {//If its a y-axis wall
                wallX = (player.xPos + ((hit.mapY - player.yPos + (1 - hit.stepY) / 2) / rayDirY) * rayDirX);
            } else {//X-axis wall
                wallX = (player.yPos + ((hit.mapX - player.xPos + (1 - hit.stepX) / 2) / rayDirX) * rayDirY);
            }
            wallX -= Math.floor(wallX);

            //x coordinate on the texture
            Texture texture = textures.get(hit.texture);
            int texX = (int) (wallX * texture.getSIZE());
            if (hit.side == 0 && rayDirX > 0)
                texX = texture.getSIZE() - texX - 1;
            if (hit.side == 1 && rayDirY < 0)
                texX = texture.getSIZE() - texX - 1;

            //calculate y coordinate on texture
            for (int y = drawStart; y < drawEnd; y++) {
                int texY = (((y * 2 - height + lineHeight) << 6) / lineHeight) / 2;
                int color;
                if (hit.side == 0) {
                    color = texture.getPIXELS()[texX + (texY * texture.getSIZE())];
                } else {
                    color = (texture.getPIXELS()[texX + (texY * texture.getSIZE())] >> 1) & 8355711; //Make y sides darker
                }

                pixels[x + y * (width)] = color;
            }
        }

        return pixels;
    }

    public RayHit castRay(double rayDirX, double rayDirY, double xPos, double yPos) {
        //length of ray from current position to next x or y-side
        double sideDistX;
        double sideDistY;
        //Length of ray from one side to next in map
        double deltaDistX = Math.sqrt(1 + (rayDirY * rayDirY) / (rayDirX * rayDirX));
        double deltaDistY = Math.sqrt(1 + (rayDirX * rayDirX) / (rayDirY * rayDirY));
        //Direction to go in x and y
        int stepX, stepY;
        int mapX = (int) xPos;
        int mapY = (int) yPos;

        //Figure out the step direction and initial distance to a side
        if (rayDirX < 0) {
            stepX = -1;
            sideDistX = (xPos - mapX) * deltaDistX;
        } else {
            stepX = 1;
            sideDistX = (mapX + 1 - xPos) * deltaDistX;
        }
        if (rayDirY < 0) {
            stepY = -1;
            sideDistY = (yPos - mapY) * deltaDistY;
        } else {
            stepY = 1;
            sideDistY = (mapY + 1 - yPos) * deltaDistY;
        }

        int side = 0; // was the wall vertical or horizontal
        boolean hit = false;
        while (!hit) {
            if (sideDistX < sideDistY) {
                sideDistX += deltaDistX;
                mapX += stepX;
                side = 0;
            } else {
                sideDistY += deltaDistY;
                mapY += stepY;
                side = 1;
            }

            if (map[mapX][mapY] > 0) hit = true;
        }

        return new RayHit(map[mapX][mapY], side, mapX, mapY, stepX, stepY);
    }

    public void updateMap(int[][] map) {
        this.map = map;
    }

}




