package main.raycasting;

public class RayHit {
    public int texture;
    public int side;
    public int mapX;
    public int mapY;
    public double stepX;
    public double stepY;

    /**
     * class containing all usefull information of a hit
     *
     * @param texture
     * @param side
     * @param mapX
     * @param mapY
     * @param stepX
     * @param stepY
     */
    public RayHit(int texture, int side, int mapX, int mapY, double stepX, double stepY) {
        this.texture = texture - 1;
        this.side = side;
        this.mapX = mapX;
        this.mapY = mapY;
        this.stepX = stepX;
        this.stepY = stepY;
    }
}
