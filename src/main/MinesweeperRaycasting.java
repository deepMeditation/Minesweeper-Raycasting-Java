package main;

import main.minesweeper.Minesweeper;
import main.raycasting.Player;
import main.raycasting.RayHit;
import main.raycasting.Screen;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class MinesweeperRaycasting extends JFrame implements Runnable {

    private static final Integer MINESWEEPER_SIZE_X = 15;
    private static final Integer MINESWEEPER_SIZE_Y = 15;
    private static final Integer MAP_SIZE_X = MINESWEEPER_SIZE_X * 2 + 3;
    private static final Integer MAP_SIZE_Y = MINESWEEPER_SIZE_Y * 2 + 3;
    private static final Integer WINDOW_WIDTH = 800;
    private static final Integer WINDOW_HEIGHT = 600;
    private static Minesweeper minesweeper;
    private int[] pixels;
    private Player player;
    private Screen screen;
    private BufferedImage image;
    private boolean exit = false;

    public MinesweeperRaycasting() {
        minesweeper = new Minesweeper(MINESWEEPER_SIZE_X, MINESWEEPER_SIZE_Y, 0.15f);
        screen = new Screen(minesweeper.getRaycastingMap(), MAP_SIZE_X, MAP_SIZE_Y, WINDOW_WIDTH, WINDOW_HEIGHT);
        image = new BufferedImage(WINDOW_WIDTH, WINDOW_HEIGHT, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        player = new Player(1.5, 1.5, 1, 0, 0, -.66);

        addKeyListener(player);
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setResizable(false);
        setTitle("Minesweeper Raycasting");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.black);
        setLocationRelativeTo(null);
        setVisible(true);

        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * player cell selection
     */
    private void selectCell() {
        RayHit hit = screen.castRay(player.xDir, player.yDir, player.xPos, player.yPos);

        if (hit.mapX > 0 && hit.mapY > 0 && hit.mapX < MAP_SIZE_X && hit.mapY < MAP_SIZE_Y && hit.mapX % 2 == 0 && hit.mapY % 2 == 0)
            minesweeper.revealCell(hit.mapX / 2 - 1, hit.mapY / 2 - 1);
    }

    public void run() {
        long lastTime = System.nanoTime();
        final double frameTime = 1000000000 / 90;
        final double physicTime = 1000000000 / 30;
        double frameDelta = 0;
        double physicsDelta = 0;

        requestFocus();

        while (!exit) {
            while (minesweeper.getResult() == 0) {
                long now = System.nanoTime();
                frameDelta += (now - lastTime);
                physicsDelta += (now - lastTime);
                lastTime = now;

                while (frameDelta >= frameTime) {
                    screen.updateMap(minesweeper.getRaycastingMap());
                    screen.update(player, pixels);
                    frameDelta -= frameTime;
                }

                while (physicsDelta >= physicTime) {
                    if (player.selectCell) {
                        selectCell();
                        player.setSelectCell(false);
                    }
                    player.update(minesweeper.getRaycastingMap());
                    physicsDelta -= physicTime;
                }

                render();
            }

            if (minesweeper.getResult() == 1) {
                System.out.println("win");
            } else {
                System.out.println("lose");
            }

            minesweeper.reset();
        }
    }

    private void render() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();
        g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        bs.show();
    }

    public static void main(String[] args) {
        new MinesweeperRaycasting();
    }
}
